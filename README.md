# Stenkoll
Simple dashboard that gives agile teams an overview over ongoing and upcoming releases and sprints, what releases are in which environment, upcoming events and who does what.

* Currently in MOC/POC state
* Read data from db is planned and partially implemented

![alt text](img/stenkoll.png "Stenkoll GUI")

## To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
git clone https://gitlab.com/mrAnkan/stenkoll.git
# Go into the repository
cd stenkoll
# Install dependencies
npm install
# Run the app
npm start
```
