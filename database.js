const path = require('path')

var exports = module.exports = {}

var knex = null;

exports.current = function() {
  return knex
}

exports.initDatabase = function() {
  if (!knex) {
    knex = require("knex")({
      client: "sqlite3",
      useNullAsDefault: true,
      connection: {
        filename: path.join(__dirname, 'database.sqlite')
      }
    })
  }
}

exports.setupDatabaseTables = function() {
  if (!knex) {
    throw new Error('Database must be initialized before any setup of tables can run!')
  }

  knex.schema
    .hasTable('sprint_status').then(async function(exists) {
      if (!exists) {
        await knex.schema
          .createTable('sprint_status', table => {
            table.increments('id')
            table.string('name')
          })
          
        return await knex('sprint_status').insert([
          { name: 'kommande' },
          { name: 'pågående' },
          { name: 'avslutad' }
        ])
      }
    })
    .catch(error => {
      console.error(error)
    })

  knex.schema
    .hasTable('sprint').then(async function(exists) {
      if (!exists) {
        return await knex.schema
          .createTable('sprint', table => {
            table.increments('id')
            table.string('name')
            table.date('start')
            table.date('end')
            table
              .integer('sprint_status_id')
              .unsigned()
              .references('sprint_status.id')
          })
      }
    })
    .catch(error => {
      console.error(error)
    })

  knex.schema
    .hasTable('release').then(async function(exists) {
      if (!exists) {
        return await knex.schema
          .createTable('release', table => {
            table.increments('id')
            table.string('name')
          })
      }
    })
    .catch(error => {
      console.error(error)
    })

  knex.schema
    .hasTable('releases_in_sprints').then(async function(exists) {
      if (!exists) {
        return await knex.schema
          .createTable('releases_in_sprints', table => {
            table
              .integer('release_id')
              .unsigned()
              .references('release.id')
            table
              .integer('sprint_id')
              .unsigned()
              .references('sprint.id')
          })
      }
    })
    .catch(error => {
      console.error(error)
    })

  knex.schema
    .hasTable('event').then(async function(exists) {
      if (!exists) {
        return await knex.schema
          .createTable('event', table => {
            table.increments('id')
            table.string('title')
            table.date('date')
            table.dateTime('time')
            table
              .integer('merge_destination')
              .unsigned()
              .references('branch.id')
          })
      }
    })
    .catch(error => {
      console.error(error)
    })

  knex.schema
    .hasTable('events_for_releases').then(async function(exists) {
      if (!exists) {
        return await knex.schema
          .createTable('events_for_releases', table => {
            table
              .integer('release_id')
              .unsigned()
              .references('release.id')
            table
              .integer('event_id')
              .unsigned()
              .references('event.id')
          })
      }
    })
    .catch(error => {
      console.error(error)
    })

  knex.schema
    .hasTable('assignee').then(async function(exists) {
      if (!exists) {
        return await knex.schema
          .createTable('assignee', table => {
            table.increments('id')
            table.string('name')
          })
      }
    })
    .catch(error => {
      console.error(error)
    })

  knex.schema
    .hasTable('branch').then(async function(exists) {
      if (!exists) {
        await knex.schema
          .createTable('branch', table => {
            table.increments('id')
            table.string('name')
          })

          return await knex('branch').insert([
            { name: 'master' },
            { name: 'develop' }
          ])
      }
    })
    .catch(error => {
      console.error(error)
    })

  knex.schema
    .hasTable('branch').then(async function(exists) {
      if (!exists) {
        await knex.schema
        .createTable('assignees_for_events', table => {
          table
            .integer('assignee_id')
            .unsigned()
            .references('assignee.id')
          table
            .integer('event_id')
            .unsigned()
            .references('event.id')
        })
      }
    })
    .catch(error => {
      console.error(error)
    })
}