const { BrowserWindow, ipcMain } = require('electron')

module.exports = class WindowManager {
  constructor() {
    this.windows = {}
  }

  createWindow(name, file, devTools, settings, menu, performWhenClose) {
    const window = new BrowserWindow(settings)

    window.setMenu(menu)
    window.loadFile(file)

    window.on('close', (event) => {
      if (performWhenClose) performWhenClose(name, event, window)

      this._unregisterWindow(name)
    })

    if (devTools) window.webContents.openDevTools()

    this.windows[name] = window
  }

  registerListenerForWindow(window, event, perform) {
    ipcMain.on(event, async () => {
      perform(this.windows[window])
    })
  }

  _unregisterWindow(name) {
    delete this.windows[name]
  }
}