const { app, BrowserWindow, Menu, ipcMain } = require('electron')
const path = require('path')

const database = require('./database.js')
const WindowManager = require('./window-manager.js')
const isMac = process.platform === 'darwin'

const template = [
  {
    label: 'Fil',
    submenu: [
      { 
        label: 'Välj databas',
        click() {
          console.log('Select database')
        } 
      },
      { 
        label: 'Ny sprint',
        click() {
          createSprintCreationWindow()
        } 
      },
      { 
        label: 'Ny version',
        click() {
          createReleaseCreationWindow()
        } 
      },
      isMac ? { label: 'Avsluta', role: 'close' } : { label: 'Avsluta', role: 'quit' }
    ]
  }
]

const windows = new WindowManager()
const menu = Menu.buildFromTemplate(template)
const env= process.env.NODE_ENV || 'development';
const develop = env === 'development'

function createMainWindow() {
  windows.createWindow('main', 'index.html', develop, {
    width: 800,
    height: 600,
    webPreferences: {
      contextIsolation: true,
      preload: path.join(__dirname, 'index-preload.js')
    }
  },
  menu
  )
}

function createSprintCreationWindow() {
  windows.createWindow('sprint-creation', 'manage-sprint.html', develop, {
    width: 320,
    height: 500,
    webPreferences: {
      contextIsolation: true,
    }
  },
  null)
}
function createReleaseCreationWindow() {
  windows.createWindow('release-creation', 'manage-release.html', develop, {
    width: 320,
    height: 500,
    webPreferences: {
      contextIsolation: true,
    }
  },
  null)
}

app.whenReady().then(() => {
  
  createMainWindow()
  database.initDatabase()
  database.setupDatabaseTables()
  
  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createMainWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  if (isMac) app.quit()
})

windows.registerListenerForWindow('main', 'mainWindowLoaded', async function (mainWindow) {
  const data = await database.current().select().from('sprint_status')
  mainWindow.webContents.send('getSprintDetails', data);
})